import React from 'react';

import Button from '@material-ui/core/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './Card.css';

const styles = {
  widthStyle: {
    width: '70%',
  },
  paddingLeft: {
    paddingLeft: '4px',
  },
}

export default class Card extends React.Component {
  constructor(props) {
    super(props);    
    this.state = {
      showBody: props.showBody,
      loveCount: props.loveCount,
    };
  }

  hideCardBody = () => {
    this.setState(prevState => ({
      showBody: !prevState.showBody,
    }));
  }

  loveThis = () => {
    this.setState(prevState => ({
      loveCount: prevState.loveCount + 1,
    }));
  }

  render() {
    const upArrow = <FontAwesomeIcon icon="chevron-up"/>
    const downArrow = <FontAwesomeIcon icon="chevron-down" />
    const avatar = <FontAwesomeIcon icon="user-circle" size="3x" color="green" />
    const { showBody, loveCount } = this.state;
    const { imgSource } = this.props;

    return (
      <div className="card">
        <div className="card-header">
          {avatar}
          <p style={styles.widthStyle}>
            Card from Dev Newbie
            Cillum aute do nulla amet in ut sunt irure.
          </p>
          <Button
            onClick={this.hideCardBody}
          >
            {showBody ? upArrow : downArrow}
          </Button>
        </div>
        {showBody && (
          <React.Fragment>
            <div className="card-body">
              <div className="card-image-body">
                <img 
                  src={imgSource}
                  width="400"
                  height="400"
                  alt="new"
                />
                <p>
                  This is description of this image.
                  loremMagna aliquip amet velit officia.
                </p>
              </div>
            </div>
            <div className="card-action">
              <Button onClick={this.loveThis}>
                <FontAwesomeIcon icon="heart" color="red" />
                <span style={styles.paddingLeft}>{loveCount}</span>
              </Button>
            </div>
          </React.Fragment>
        )}
      </div>
    )
  }
}