import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fab } from '@fortawesome/free-brands-svg-icons';
import {
  faChevronUp,
  faChevronDown,
  faUserCircle,
  faHeart,
} from '@fortawesome/free-solid-svg-icons';

import Card from './components/Card';
import './App.css';

library.add(
  fab,
  faChevronUp,
  faChevronDown,
  faUserCircle,
  faHeart,
);

const muiStyle = {
  cardStyle: {
    width: '30%',
    height: '250px',
  },
}

class App extends Component {
  state = {
    cardInfo: [{        
        showBody: true,
        loveCount: 0,
        imgSource: "https://images.pexels.com/photos/20787/pexels-photo.jpg?auto=compress",
      }, {        
        showBody: true,
        loveCount: 0,
        imgSource: "https://cdn.pixabay.com/photo/2018/07/28/15/02/sheep-3568254_960_720.jpg",
      },
    ],
  };

  render() {
    const { cardInfo } = this.state;

    return (
      <div className="card-box">
        {cardInfo.map(({ showBody, loveCount, imgSource }, index) => (
          <Card
            showBody={showBody}
            loveCount={loveCount}
            imgSource={imgSource}
          />
        ))}
      </div>
    );
  }
}

export default withStyles(muiStyle)(App);
